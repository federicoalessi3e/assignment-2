# Assignment 2

This project stands for the second assignment for the course "Processo e sviluppo
del software" at Milano's Bicocca University.

## Project's summary ##

My idea is to develop a web application for elementary schools' students to let them
learn English in a better and different way. The app will allow both students, teachers and
parents to share a common dashboard in which teachers can send exercises and 
other material of the course and students will be able to access them from home.

The user (the children) will access the app with a unique login, and once they will be logged, he
could have vision of his/her own dashboard. There all the different session will be
displayed:

* <b> exercises </b>: where the teachers can send them homework
* <b> dictionary </b>: a section in which children can write down new words they learn
* <b> announcements </b>: where teacher can put posts or write a direct message to a single user.

The profile is meant to be for both children and parents: the users, being children
with age between 6 and 10 years old, can have difficulties by interacting with technology.
Therefore parents have to help them in discovering the different features the app can offer
and, furthermore, they have help them by doing exercises with them (if possible).

Teachers will access in a different dashboard that will allow them to send exercises, and
write message to the class or to a group of students (one or more).
Each teacher will be able to interact only with his/her own classes, through a unique code
that must be inserted in the set up of his/her profile. 

<b> I'd like to introduce many more features, but at the moment I have only thought
about the ones I reported above </b>.

*The goal of this project is to improve italian-children-English-level since the
early school*.

## Assumptions ##

I'm assuming that my project has been asked by the mayor of a city to promote an
educational/innovative project in his town and, if the project is actuable and
easily integrable with a primary school system, it can be exported in other towns.

For this reason I assumed that it is developed starting only for just one school: 
this will cause that the stakeholder included in the project won't be a moltitude 
of headmasters and a great amount of teachers but just one headmaster (the one in charge
in the school of the town that asked me the project) and just the English teachers of that
school.
<b>Moreover I'm assuming that the mayor has all the municipal and regional permission
to carry out this project</b>.

Another assumption has been made for the investors: the money for the project will be
given by the town or the province, so the figure of the 'investor' will be, in a certain way, incorporated
by the mayor, that will have the task to follow the project for his personal promotional
initiative but also will have to make sure that the project will be really usefull
and effective for the community it will be designed.

## Stakeholders ##
The most important stakeholder of the project is the mayor: he's got high interest in
it as it has been requested from him and he has high power too (he can take the most
important decisions, last-minute changes etc etc. ). For this reason he's a <b>key stakeholder</b>.

As this proejct concerns school, it will include headmaster and teachers as
stakeholders. They are the actuators of the project because they live in the school
system and are the figures in contact with the final users: the students. For this 
reason, even if they have no will in joining the project, they have to be interested in
it, because they will be affected by the changes the application will bring in the
school-system. On the contrary, their decisional power is less then the mayor's one:
they can propose ideas and new features but if they mayor takes a decision about something in the project they
can't reply or refuse to follow it. For this reason both headmaster and teachers are
<b>operative stakeholders</b>.

For what concerns parents and children, they are part of the community that will be
affected by the project. So they have to be included as stakeholder of the project.
Both are <b>operative stakeholders</b>.

Furthermore, another stakeholder included will be the team project, the people who
will develop the software.

Finally, another stakeholder of my project can be the investors, that are the third
party that will be interested in the financial results of it. 
They may also have the right to approve or reject major decisions.
Actually the investors would be key stakeholders. As I said in the 'Assuptions' paragraph
the investors will be impersonificate behind the figure of the mayor, that will follow
for them that the project is proceeding in the right way.

Customers (who requested it)

* mayor (<b>key</b>)


Community (who will be affected by the project)

* parents (<b>operative</b>)
* child (<b>operative</b>)
* teachers (<b>operative</b>)
* headmaster (<b>operative</b>)

Internal stakeholder

* team members (<b>operative</b>)

Owner of the business (interested in the financial part of the project)
* investors (<b>key</b>)


## Pitch Deck for investors ##

To introduce the project to investors (mayor and regional delegates) I will use
a pitch deck presentation.

A pitch deck is a brief presentation, used to provide your audience with a quick
overview of your business plan. It will usually be presented during face-to-face 
or online meetings with potential investors, customers, partners, and co-founders.

The presentation is composed by two different parts: the first one deals with what the
problem is, how the project is supposed to solve it and why it should be special 
and different from other solutions already available. Examples are provided within 
this section.
The second part is related to the finantial part: a business model is shown to explain
how I expect to make money, within a request for an initial investment. The expectation
for the target market I want to reach has to be included in this section too.

Finally personal contact has to be provided at the end of the presentation to let
investors easily be able to keep in touch with the team in case they wanted to give
money to start the project.


## Elicitation strategies and dependencies between them ##

### Background study ###

The background study I would do for this project could be composed by different
parts:

+ the first one is a <b>data collection</b> to take some informations about the primary school
students' English-marks of the previous years to compare them with the average in
Europe. This could give a better idea about the level of the English teaching in the
Italian primary school I chose as a test.

+ the second one is a research about which arguments are taught in primary schools and
how they are taught. In this part I will need the help of the English teachers that can
give me informations based on their own years of teaching. Having more teacher could be
helpfull because they can have different experiences and different methods, so the context
can be seen with different points of view

+ another part of the background study could be the collection of the standards and
the laws that regulate the teaching in primary schools. This collection will output
what can be done and if something is not in the standards of the law.

+ Finally I would check for competitors app to see how they deal with the problem.
In particular this would provide a starting point for the feature the project has to have and
the solutions already proposed by others. 

Once the background study is completed I will have a great amount of information 
that will allow me to interact better with the involved stakeholders, paving the way
of the other elicitation strategies.

### Questionnaire ###

The questionnaire I would design for this project is aimed to the parents of the children,
as they are not involved directly in the school systems. The questionnaire will be needed to
know what they think about the project, if they're available to help their sons with English
and so on.
To reach parents' attention teachers has to inform them via children'diaries, which are often
checked by parents once they come back home. The questionnaire will be available on Google Form and parents
just has to spend less then 5 to answer the few questions in it.

Clearly there's the possibility that they won't respond to the quetionnaire, but the stimulation to answer is that 
with this project their sons'English level can be improved and children could like studying with the application, 
within their parents' help, more than studying at school.

The questionnaire is divided in group of questions and can be found 
at this [link](https://goo.gl/forms/DhEZnQnokZEzFpY53). 

Before the questions I explained whats' the purpose of them and I attached a link in which,
who will answer to the quetionnaire will be able see what's Italy's English level compared to Europe or world' ones.
<u>This source has been found during the data collection in the previous elicitation strategy</u>.

Talking about the structure of the questionnaire, it's composed by 13 question of which
9 are mandatory. Some of them are multiple-choice-questions, other have a scale of
values from 'not interested at all' to 'very interested'. To avoid 
[central tendency bias](https://www.statisticshowto.datasciencecentral.com/central-tendency-bias/)
I designed this scales with 6 values so people can't answer with the middle value if they don't know
what to answer.

The first five questions are about English and its importance at work/school. Parents
are supposed to answer if they think English is important at school, if they studied it
during their studies, and what's their English level. From this area I expect to collect
information about the attitude and the interest of parents relative to English level and
collecting information about what they think about the importance of studying English.

The second session is about internet and online formative courses. Here we have other 
5 questions that ask parents if they use internet and for what purpose and if they
have ever used websites/web application to learn something. This questions introduce
the really important one: how much they would be interested in using a web app with their 
child to integrate more material to the English lessons performed at school.
This section will collect information about the attitude of parents about their internet routines
and if they are new to online courses. This is really important because we can see if the 
type of user we're dealing with have already used something like what we are gonna propose
them with this project.

Finally we have two open questions that ask the user to insert his age and then
to insert his son surname and his class.
The first of this question will give us an idea of when the one who is responding attendended
schools to see if there is a relationship and similiarities between the answers of the first and the second section
and users in the same age range.

The second one is needed to avoid that the same parent perform two or more times 
the questionnaire.

(After I completed the questionnaire I asked some people to answer it to check
how long it was to complete it. With a sample of 25 people the average time was
2 minutes and 20 seconds).

After this strategy, the team would have more information about what parents think about
the project and what kind of 'welcome' to expect once it will be finished.


### Brainstorming session ###

Here the stakeholders meet together to argue about how the project will be like.
The things that make this strategy so important is that <b>teachers and headmasters</b> can
meet the developers of the project and give them a different point of view about the 
project. They can give the team their ideas about how the project has to be like
and which feature to include and how to include it, basing them on their school-living experience. 
At the beginning each one can propose his own thoughts with no criticism, and the output of this first part of the meeting
will be a list of ideas/features. After this the group will evaluate each point of the
list according to some criterias (value, feasibility etc..). Obviously, being the mayor
an higher-priority stakeholder and holding the main interest in the project, 
his decision may override every other new feature's idea that has been thought.

This part of the elicitation strategies will contribute to the requirements evaluation
because we will have a good starting point of what to include in the project.

Moreover another important thing of meetup sessions is that the stakeholder involved in it have the sensation
to be important for the project, creating a <b>sense of belonging</b> to it that will lead
to a <b>positive attitude</b> and a better relationship between the internal team and the
people who will be affected by the project.

### Project Mockup ###

After the brainstorming session, features of the software-to-be would have been defined.
To get an early-feedback by the stakeholder involved in the previous strategies a mock-up
of what the dashboard (both teachers and children' ones) is the better things to do.
The choice of the mockup over the prototype, at this stage of the project, is very simple:
the mock-up is quicker to be produced and it's more usefull to show, in a static way, 
an high-fidelty copy of the contents and the functions. 

If everything will be OK, the mockup will be thrown and  the project will go on to the next strategy.
On the other hand we will have another brainstorming session to elicit the reqs that are still unclear,hidden or not
satisfied by the first demonstration of the mockup.

### Scenarios ###

With the mockup I would give to the involved stakeholder a sight of what the application
will look like. To let a better understanding of the various functionality and of the workflow
of them, I would prepare a set of scenarios (both positives and negatives) to introduce them
what kind of things and situations they have to expect by working and interaction with the application. 
This can be useful, expecially with a set of stakeholders as mine: in fact, often primary school
teachers hasn't acquired skills in the usage of softwares and showing them how things work and
what can go wrong during a work session can be educational and ease them their user experience.

### Prototype aimed to children ###

After an intence elicitation session with the 'adult's world', it's time to pass 
to the real user of the application: young students. It's not quite easy to find a good 
elicitation strategies for them because of their ages. The best way to do so is to
introduce them to something they can interact with and that may catch their attention:
for this reason, I think that a simple prototype of the final application is the best idea.

At this stage of the project prototyping is a good idea also for other reasons:
 * it's the product of an entire elicitation workflow that has gone through different
phases and it can show how much the gaining of informations is aligned with stakeholders'
ideas and request.

 * if the prototype is a good one, the team will be able to reuse the code for the software 
development phase, saving time and having a good starting point that will ease that phase
of the project.

The prototype presented to children will test the UX of the app to check how intuitive it is,
if something else could be added, removed or changed. The application that will be tested
will include the three main area (homework, dictionary, announcments) with reduced functionalities
sufficient to complete three task presented to students.

The main aspect on which I would focused is <b>intuitiveness</b> for the user of the application's
age: being them children will involve that the application must be easily usable and with really
simple tasks, avoiding ambiguity. If this constraint won't be respected the application
will have an opposite effect on students' learning.

<b> I assumed to test the prototype by presenting it to only a class for every 
year: being italian school sistem to 1st class to 5th, five class will test 
the prototype. The only change of this will be the test-homework student will do in task 1</b>.

Three simple tasks will be given to students to test the intuitiveness:

* open the 'exercises' area and open up an homework in there (the homework will be suited for 
the accademic year of the classroom chosen) and complete it by writing something on it and submitting it.
* come back to the dashboard and check which words are saved in the dictionary and learn one of it.
* come back to the dashboard and check if there's a new message from the teacher in the 'message' area.

Parents can give a little help to their sons to complete these tasks, because the application is designed
to make them cooperate with studends, making the user experience even more simple.


### Gaining children feedback by questionnaire ###

At this final stage of the elicitation strategy I would have had an interview with the students
who tested the prototype: however, dued to the high number of students (average of 20 students per class per 5 class = 100 interviews)
the necessary time to interview everybody would have been too high, even if the interview would have been performed to a sample of them
for each class.
For this reason I chose to gain their feedback with a questionnaire:
it will include:

* group of questions relative to school, English and technology and
how much they like each of these things to 1('I really don't like it') to 5('I like it very much'). 
This will help to understand ifthe app will be only a waste of time and a boring things to do for them, or if it will
be effective and embroiling.
* another group of questions will ask how easily they completed their task in a scale from 
1(too hard) to 5 (very easy) and, if the answer will be 1 they will be asked why they have had
difficulties
* finally two open questions in which they will be asked to propose something that still miss
in the application and which word they learned in the dictionary section of task 2, writing it
in both English and Italian.

These two questions have a really high importance for the project: the first one will
make them feel involved in an application designed for them (even if the proposals won't be
included in the final version of the app). And the second one will only show if the 
dictionary section is usefull to them to learn new English words, giving a further
feedback of the effectivness (or not) of the application.

Once all the results of the quetionnaire have been collected and analyzed, conclusions
can be infered: if the feedbacks are positive the project can go on with the next phase.
On the other hand, it will be discussed again among the stakeholders to change and being aligned
to users' feedbacks.







